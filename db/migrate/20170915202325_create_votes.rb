class CreateVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :votes do |t|
      t.references :movie
      t.references :user
      t.boolean :like
      t.boolean :hate

      t.timestamps
    end
  end
end
