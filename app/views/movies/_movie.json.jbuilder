json.extract! movie, :id, :title, :description, :published_at, :created_at, :updated_at
json.url movie_url(movie, format: :json)
