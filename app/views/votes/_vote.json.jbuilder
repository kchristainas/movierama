json.extract! vote, :id, :type, :like, :hate, :created_at, :updated_at
json.url vote_url(vote, format: :json)
