module MoviesHelper

  def get_user_nickname(movie)
    return '' unless movie.class == Movie
    movie.user == current_user ? 'You' : movie.user.nickname
  end
end
