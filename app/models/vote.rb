class Vote < ApplicationRecord
  belongs_to :user
  belongs_to :movie

  scope :likes, -> { where(like: true) }
  scope :hates, -> { where(hate: true) }
  scope :active, -> { where(hate: true).or(Vote.where(like: true)) }
end
