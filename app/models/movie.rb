class Movie < ApplicationRecord
  has_many :votes
  belongs_to :user

  scope :ordered_by_likes,  -> { joins("LEFT JOIN votes ON votes.movie_id = movies.id").order("votes.like desc, movies.votes_count desc").uniq }
  scope :ordered_by_hates,  -> { joins("LEFT JOIN votes ON votes.movie_id = movies.id").order("votes.hate desc, movies.votes_count desc").uniq }
  scope :ordered_by_date,   -> { order(created_at: :asc) }

  # Returns count of likes, hates or all votes depending on `type` parameter.
  # By default returns `active` count.
  def votes_count(type = 'active')
    votes.send(type).count
  end

  def has_votes?(type = 'active')
    !votes_count(type).zero?
  end
end
