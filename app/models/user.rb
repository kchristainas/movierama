class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :votes
  has_many :movies

  validates :first_name, presence: true
  validates :last_name, presence: true

  def nickname
    @nickname ||= [first_name, last_name].join(" ")
  end

  def likes_movie?(movie)
    votes.likes.map(&:movie).include?(movie)
  end

  def hates_movie?(movie)
    votes.hates.map(&:movie).include?(movie)
  end
end
