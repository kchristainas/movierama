Rails.application.routes.draw do

  resources :votes
  devise_for :users
  root to: 'movies#index'

  resources :movies

  resources :users do
    resources :movies
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
